<section class="content-header">
    <h1>User Listings</h1>
</section>
<section class="content">
    <table id="users-table" class="table table-bordered">
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Email</th>
                <th>Created At</th>
            </tr>
        </thead>
        <tbody>
            @foreach($searchedUsers as $user)
            <tr>
                <td>{{$user->fb_user_id}}</td>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->created_at->diffForHumans()}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
    {!! $searchedUsers->render() !!}
</section>
@extends('layouts.master')
@section('content')
<div class="container">
    <div class="col-sm-12">
        @if(isset($response['error']))
        <div class="alert alert-danger">
            {{$response['error_msg']}}
        </div>
        @endif
    </div>
    <div class="col-sm-6 col-sm-offset-5">
        <a class="btn btn-primary" style="margin-top: 60%;" href="{{isset($loginUrl) ? $loginUrl : ''}}">Login using Facebook</a>
    </div>
</div>

@endsection
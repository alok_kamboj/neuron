<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{asset('img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{Auth::check() ? Auth::user()->name : ''}}</p>
            </div>
        </div>

        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>

            <li class="active">
                <a href="{{ route('profile') }}">
                    <i class="fa fa-dashboard"></i> <span>Profile</span>
                </a>                
            </li>
            <li>
                <a href="{{ route('freind.requests') }}">
                    <i class="fa fa-dashboard"></i> <span>Friend Requests</span>
                </a>                
            </li>
            <li>
                <a href="{{ route('sendmail') }}">
                    <i class="fa fa-dashboard"></i> <span>Send Mail</span>
                </a>                
            </li>
            <li>
                <a href="{{ route('logout') }}">
                    <i class="fa fa-dashboard"></i> <span>Logout</span>
                </a>                
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
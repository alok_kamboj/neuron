<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Test Task</title>
        <!-- Bootstrap -->
        <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/admin/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/admin/ionicons.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/admin/AdminLTE.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/admin/_all-skins.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/admin/morris.css') }}">
        <link rel="stylesheet" href="{{ asset('css/admin/jquery-jvectormap.css') }}">
        <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/daterangepicker.css') }}">
        <link rel="stylesheet" href="{{ asset('css/admin/bootstrap3-wysihtml5.min.css') }}">
        <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css" >
        <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
</head>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
        @if(Auth::check())
        @include('layouts.sidebar')
        @endif
        @yield('content')
    </div>

    <!--<script src="{{ asset('js/jquery.min.js') }}"></script>-->
    <script src="//code.jquery.com/jquery-1.10.2.min.js"></script>

    @yield('footer-scripts')
</body>
</html>
@extends('layouts.master')
@section('content')
<div class="wrapper">
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Send Mail</h1>
        </section>
        <section class="content">
            @include('flashmsg.message')
            {!! Form::open(['route' => 'sendmail.post', 'method' => 'POST']) !!}
            <div class="form-group">
                <label for="email">To:</label>
                {!! Form::text('email',null,['class' => 'form-control', 'placeholder' => 'To mail address']) !!}
            </div>
            <div class="form-group">
                <label for="subject">Subject:</label>
                {!! Form::text('subject',null,['class' => 'form-control', 'placeholder' => 'Subject']) !!}
            </div>
            <div class="form-group">
                <label for="body">Message:</label>
                {!! Form::textarea('message',null,['class' => 'form-control', 'placeholder' => 'Message']) !!}
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
            {!! Form::close() !!}
        </section>
    </div>
</div>
@endsection
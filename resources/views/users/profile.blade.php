@extends('layouts.master')
@section('content')
<div class="wrapper">
    <div class="content-wrapper">
        <section class="content-header">
            <h1>User Profile</h1>
        </section>
        <section class="content">
            <div class="col-md-12 pull-right">
                {!! Form::open(['route' => 'profile', 'method'=>'GET']) !!}
                <div class="col-md-6 pull-left">
                    {!! Form::text('keyword',isset($keyword) && !empty($keyword) ? $keyword : '',['class' => 'form-control', 'placeholder' => 'Search Users']) !!}
                </div>
                <div class="col-md-3 pull-left">
                    <button class="btn btn-primary" type="submit">Search</button>
                </div>
                {!! Form::close() !!}
            </div>
            @include('flashmsg.message')
            <table id="users-table" class="table table-bordered">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Created At</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($user) > 0 )
                    <tr>
                        <td>{{$user->fb_user_id}}</td>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->created_at->diffForHumans()}}</td>
                    </tr>
                    @else
                    <tr>
                        <td colspan="4">No Result</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </section>
        <hr>
        @if(count($searchedUsers) > 0)
        @include('partials.search_users', ['searchedUsers' => $searchedUsers])
        @endif
    </div>
</div>
@endsection


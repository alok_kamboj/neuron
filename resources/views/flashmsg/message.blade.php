@if (session('status'))
<div class="alert alert-info">
    {{ session('status') }}
</div>
@endif
@if (count($errors) > 0)
<div class="alert alert-danger global_messenger">
    <!--Please correct below errors.<br><br>-->
    <ul>
        <li><strong class="fa fa-exclamation-triangle"></strong> Please correct below errors</li>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
@if(Session::has('message'))
<p class="alert {{ Session::get('alert-class', 'alert-success') }}">{{ Session::get('message') }}</p>
@endif
@if (session()->has('error'))
<div class="col-sm-12" style="margin-top:30px;">
    <div class="alert alert-danger">
        <div>{{ session('error') }}</div>
    </div>
</div>

@endif
@if (session()->has('success'))
<div class="col-sm-12" style="margin-top:30px;">
    <div class="alert alert-success">
        <div>{{ session('success') }}</div>
    </div>
</div>
@endif
<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\User;
use Auth;
use App\Libraries\FacebookLibrary;
use App\Libraries\SendMail;

class SocialMediaController extends Controller {

    protected $fbLibObj;

    /**
     * Constructor to initialize the FB object with config
     */
    public function __construct() {
        $this->fbLibObj = new FacebookLibrary();
    }

    /**
     * Method to show login page which will have FB login Url
     * @return type
     */
    public function index() {
        $loginUrl = $this->fbLibObj->login();
        return view('welcome')->with(compact('loginUrl'));
    }

    /**
     * Method to receive a callback from Facebook after login and update or create user
     * @return type
     */
    public function facebookCallback() {
        $this->logout();
        $response = $userInfo = array();
        $response = $this->fbLibObj->facebookCallback();
        if (isset($response['error']) && $response['error'] == 1) {
            return view('welcome')->with(compact('response'));
        }
        $userInfo = $this->fbLibObj->getUserInfo($response['token']);
        $user = User::where('email', $userInfo['body']['email'])->first();
        if (count($user) > 0) {
            $user->name = $userInfo['body']['name'];
            $user->email = $userInfo['body']['email'];
            $user->fb_user_id = $userInfo['body']['id'];
            $user->token = $response['token'];
            $user->save();
        } else {
            $user = User::create([
                        'name' => $userInfo['body']['name'],
                        'email' => $userInfo['body']['email'],
                        'fb_user_id' => $userInfo['body']['id'],
                        'token' => $response['token']
            ]);
        }
        Auth::loginUsingId($user->id);
        return redirect()->route('profile');
    }

    /**
     * Method to get Profile of user and searched users
     * @param Request $request
     * @return type
     */
    public function getProfile(Request $request) {
        if (Auth::check()) {
            $keyword = '';
            $searchedUsers = array();
            $user = Auth::user();

            if (!empty($request->keyword)) {
                $keyword = $request->keyword;
                $searchedUsers = User::where('name', 'LIKE', '%' . $keyword . '%')
                        ->orWhere('email', 'LIKE', '%' . $keyword . '%')
                        ->orWhere('fb_user_id', $keyword)
                        ->paginate(5);
            }
            return view('users.profile')->with(compact('user', 'searchedUsers', 'keyword'));
        }
        return redirect()->route('login');
    }

    /**
     * Method to logout user
     * @return type
     */
    public function logout() {
        if (Auth::check()) {
            Auth::logout();
            return redirect()->route('login');
        }
    }

    /**
     * Method to get the mail page
     * @return type
     */
    public function getMail() {
        return view('users.mail');
    }

    /**
     * Method to send the mail using mail library class
     * @param Request $request
     * @return type
     */
    public function sendMail(Request $request) {
        $validator = Validator::make($request->all(), [
                    'email' => 'required|email',
                    'subject' => 'required',
                    'message' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }
        SendMail::sendMail($request);
        return redirect()->route('profile');
    }

    public function getFriendRequests() {
        $user = Auth::user();
        $res = $this->fbLibObj->getFriendRequests($user->token);
    }

}

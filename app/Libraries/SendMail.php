<?php

namespace App\Libraries;

use Illuminate\Support\Facades\Mail;

/**
 * Library used to handle mail events
 */
class SendMail {

    /**
     * Method to send mail
     * @param type $request
     */
    public static function sendMail($request) {
        try {
            Mail::send('emails.send_mail', compact('request'), function($msg) use($request) {
                $msg->to($request->email)->subject($request->subject)->from('test@almedicorp.com');
            });
        } catch (Exception $ex) {
            
        }
    }

}

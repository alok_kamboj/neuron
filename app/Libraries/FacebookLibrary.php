<?php

namespace App\Libraries;

session_start();
use Illuminate\Support\Facades\Session;

class FacebookLibrary {

    // We can also use config file to load the app id and secret
    protected $appId = '1840535049304123';
    protected $secret = 'ec1836ac5ff1cc5c8a44de21fe335525';
    protected $fbObj;

    public function __construct() {
        $this->fbObj = new \Facebook\Facebook([
            'app_id' => $this->appId,
            'app_secret' => $this->secret,
            'default_graph_version' => 'v2.10',
                //'default_access_token' => '{access-token}', // optional
        ]);
    }

    /**
     * Method to login using FB sdk
     * @return type
     */
    public function login() {
        $helper = $this->fbObj->getRedirectLoginHelper();
        $callbackUrl = route('fb.callback');
        $permissions = ['email', 'user_friends', 'user_events', 'read_custom_friendlists', 'manage_pages', 'public_profile', 'publish_pages', 'publish_actions', '']; // Optional permissions
        $loginUrl = $helper->getLoginUrl($callbackUrl, $permissions);

        return htmlspecialchars($loginUrl);
    }

    /**
     * Method to receive the callback from facebook after login 
     * @return type
     */
    public function facebookCallback() {
        $response = array();
        $response['error'] = 0;
        $helper = $this->fbObj->getRedirectLoginHelper();

        try {
            $accessToken = $helper->getAccessToken();
        } catch (\Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            $response['error'] = 1;
            $response['error_msg'] = $e->getMessage();
            return $response;
        } catch (\Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            $response['error'] = 1;
            $response['error_msg'] = $e->getMessage();
            return $response;
        }

        if (!isset($accessToken)) {
            if ($helper->getError()) {
                header('HTTP/1.0 401 Unauthorized');
                echo "Error: " . $helper->getError() . "\n";
                echo "Error Code: " . $helper->getErrorCode() . "\n";
                echo "Error Reason: " . $helper->getErrorReason() . "\n";
                echo "Error Description: " . $helper->getErrorDescription() . "\n";
            } else {
                header('HTTP/1.0 400 Bad Request');
                echo 'Bad request';
            }
            exit;
        }

        // Logged in
        $response['token'] = $accessToken->getValue();
        // The OAuth 2.0 client handler helps us manage access tokens
        $oAuth2Client = $this->fbObj->getOAuth2Client();

        // Get the access token metadata from /debug_token
        $tokenMetadata = $oAuth2Client->debugToken($accessToken);
        $response['meta_data'] = $oAuth2Client->debugToken($accessToken);

        // Validation (these will throw FacebookSDKException's when they fail)
        $tokenMetadata->validateAppId($this->appId);
        // If you know the user ID this access token belongs to, you can validate it here
        //$tokenMetadata->validateUserId('123');
        $tokenMetadata->validateExpiration();

        if (!$accessToken->isLongLived()) {
            // Exchanges a short-lived access token for a long-lived one
            try {
                $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
            } catch (\Facebook\Exceptions\FacebookSDKException $e) {
                $response['error'] = 1;
                $response['error_msg'] = $e->getMessage();
                return $response;
            }
            $response['token'] = $accessToken->getValue();
        }
        $sessionFbVal = (string) $accessToken;
        Session::put('fb_access_token', $sessionFbVal);
        return $response;
    }

    /**
     * Method to info for user
     * @param type $accessToken
     * @return type
     */
    public function getUserInfo($accessToken) {
        $response = array();
        $response['error'] = 0;
        //Get user Info
        if (!empty($accessToken)) {
            try {
                // Returns a `Facebook\FacebookResponse` object
                $res = $this->fbObj->get('/me?fields=id,name,email', $accessToken);
            } catch (\Facebook\Exceptions\FacebookResponseException $e) {
                $response['error'] = 1;
                $response['error_msg'] = $e->getMessage();
            } catch (\Facebook\Exceptions\FacebookSDKException $e) {
                $response['error'] = 1;
                $response['error_msg'] = $e->getMessage();
            }
            $response['body'] = $res->getDecodedBody();
        }
        return $response;
    }

    public function getFriendRequests($accessToken) {

        if (!empty($accessToken)) {
//            $res = $this->fbObj->get('/me/friendrequests', $accessToken);
//            $res = $this->fbObj->get('/me?fields=friends.fields=(first_name)', $accessToken);
//            $res = $this->fbObj->get('/1821497045?fields=context.fields(all_mutual_friends)', $accessToken);
            $res = $this->fbObj->get('/1821497045', $accessToken);
            dd($res->getDecodedBody());
        }
    }

}

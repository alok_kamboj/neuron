<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', 'SocialMediaController@index')->name('home');
Route::get('/', 'SocialMediaController@index')->name('login');
Route::any('/callback', 'SocialMediaController@facebookCallback')->name('fb.callback');


Route::group(['middleware' => 'auth'], function () {
    Route::get('/profile', 'SocialMediaController@getProfile')->name('profile');
    Route::get('/logout', 'SocialMediaController@logout')->name('logout');
    Route::get('/send/mail', 'SocialMediaController@getMail')->name('sendmail');
    Route::post('/send/mail', 'SocialMediaController@sendMail')->name('sendmail.post');
    Route::get('/friend/requests', 'SocialMediaController@getFriendRequests')->name('freind.requests');
});

//Route::get('/', function () {
//    return view('welcome');
//});
